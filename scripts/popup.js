var DEBUG = true;

// TODO: Fix this
function err_needLogin() {
    setMessageDiv("Could not load class list, please <a href='" + MYPLACE_URL + "/login/'>log in to MyPlace</a>.");
}

function setClassesDiv(val) {
    $("#classlist_content").html(val);
}

function setMessagesDiv(val) {
    $("#messages_content").html(val);
}

function initClassList() {
    var raw = local_getClassList();
    var classes = JSON.parse(raw);

    if (classes == null) {
        // If class data is missing from local storage try to scrape it
        myplace_loadClassInfo();
    } else {
        setClassesDiv(createClassListHTML(classes));
    }
}

function initMessages() {
    if (local_hasMessageList() === false) {
        myplace_loadMessageInfo(false);
    } else {
        myplace_loadMessageInfo(true);
        var messages = JSON.parse(local_getMessageList()).messages;
        setMessagesDiv(createMessageListHTML(messages));
    }

    // Re-bind the click handler for MyPlace link divs, otherwise the message
    // divs won't respond to clicks
    bindClicks();
}

function initContent() {   
    initClassList();
    initMessages();
}

// Entry point when document loads
$(document).ready(function() {
    initContent();
    bindClicks();
});

function bindClicks() {
    $(".mplink").unbind("click");
    $(".mplink").bind("click", function (event) {
        var div = event.currentTarget;
        var targetURL = $(div).attr("href");

        chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
            var tab = tabs[0];

            if (tab.url.indexOf("http://classes.myplace.strath.ac.uk/") > -1) {
                // MyPlace tab is currently selected, "replace it"
                chrome.tabs.update(tab.id, {url: targetURL});
            } else {
                // MyPlace tab not currently selected, create a new tab
                chrome.tabs.create({url: targetURL});
            }
        });
    });
}