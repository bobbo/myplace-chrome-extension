function fillPage() {
    if (local_hasClassList()) {
        $("#local").html("You have class list information stored locally. <br><input type='button' id='clear_classlist_btn' value='Clear Data'/>");
    } else {
        $("#local").html("You have no class data stored locally");
    }
}

$(document).ready(function () {
    fillPage();
    $("#version").append(" " + chrome.runtime.getManifest().version);

    $("#clear_classlist_btn").click(function () {
        local_clearClassList();
        fillPage();
    });
});
