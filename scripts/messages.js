var MSG_URL = MYPLACE_URL + "/mod/strathcom/view.php";
var MSG_VIEW_URL = MYPLACE_URL + "/mod/strathcom/viewitem.php?id=";
var MSG_SHOW_COUNT = 8;

function messageStore_create(messages, recentID) {
    return {most_recent: recentID, messages: messages};
}

function message_create(id, title, classCode, read) {
    return {id: id, title: title, classCode: classCode, read: read};
}

function message_getLI(message) {
    var code = (message.classCode) ? "[" + message.classCode + "] " : "";
    var div = "<div class='mplink message' href='" + MSG_VIEW_URL + message.id + "'>" + code + message.title + "</div>";
    var liClass = "msglink" + ((message.read) ? "" : " unread");

    return "<li class='" + liClass + "'>" + div + "</li>";
}

function myplace_getMessageIDFromURL(url) {
    var values = url.split("=");
    return values[1].split("&")[0];
}

function myplace_parseMessageInfo(data) {
    var messages = [];

    $(".r1", data.target.responseText).each(function () {
        // TODO: Can these jQuery selectors be improved?
        var messageLink = $(".cell.c0", $(this)).children("a");
        var messageID = myplace_getMessageIDFromURL(messageLink.attr("href"));
        var messageSubject = messageLink.html();
        var messageRead = (messageLink.attr("class") !== "unread");

        var courseLink = $(".cell.c3", $(this)).children("span").children("a");
        var messageClassCode = courseLink.text().split(":")[0];

        messages.push(message_create(messageID, messageSubject, messageClassCode, messageRead));
    });

    var msgStore = messageStore_create(messages, messages[0].id);
    local_setMessageList(msgStore);
    initMessages();
}

function myplace_loadMessageInfo(silent) {
    if (silent == false)
        setMessagesDiv("Requesting message data <img src='../loading.gif'></img>");

    // TODO: Error handler
    httpGet(MSG_URL, myplace_parseMessageInfo, function (d) {
        console.log(d);
    });
}

function createMessageListHTML(messages) {
    var buffer = ["<ul class='messagelist'>"];

    for (var i = 0; i < MSG_SHOW_COUNT; i++) {
        buffer.push(message_getLI(messages[i]));
    }

    buffer.push("</ul>");
    return buffer.join("");
}