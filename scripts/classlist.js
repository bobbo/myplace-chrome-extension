function class_create(code, name, id) {
    return {code : code, name : name, id : id};
}

function class_getURL(class_data) {
    return MYPLACE_URL + MYPLACE_VIEW_CLASS + class_data.id;
}

function class_getLI(class_data) {
    var code = (class_data.code) ? "[" + class_data.code + "]" : "";
    return "<li><div class='mplink class' href='" + class_getURL(class_data) + "'>" + code + " <strong>" + class_data.name + "</strong></div></li>";
}

function myplace_scrapeClasses(doc) {
    var classes = [];

    $(".type_course.depth_3", doc).each(function () {
        var anchor = $("a", $(this));
        var title = anchor.attr("title");
        var code = "";
        var name = "";

        if (title.indexOf(":") != -1) { // colon is present
            var chunks = title.split(":");
            code = chunks[0];
            name = $.trim(chunks[1]);
        } else {
            name = title;
        }

        var link = anchor.attr("href");
        var id = link.split("=")[1];

        classes.push(class_create(code, name, id));
    });

    return classes;
}

function myplace_parseClassInfo(data) {
    setClassesDiv("Parsing class data");

    var classes = myplace_scrapeClasses(data.target.responseText);

    if (classes.length > 0) {
        local_storeClassList(classes);
        initClassList();
    } else {
        console.log("This is a major error");
    }
}

function myplace_loadClassInfo() {
    setClassesDiv("Requesting class data <img src='../loading.gif'></img>");
    httpGet(MYPLACE_URL + "/my/", myplace_parseClassInfo, err_needLogin);
}

function createClassListHTML(list) {
    if (list == null)
        return "<b>No classes to show</b>";

    var buffer = ["<ul id='classlist'>"];
    for (var i = 0; i < list.length; i++) {
        buffer.push(class_getLI(list[i]));
    }

    buffer.push("</ul>");
    return buffer.join("");
}