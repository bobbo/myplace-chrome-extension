var STORE_CLASSLIST = "mpext_class_list";
var STORE_MESSAGES = "mpext_message_list";

function local_clearAll() {
    local_clearClassList();
    local_clearMessageList();
}

function local_hasClassList() {
    return (local_getClassList() !== null);
}

function local_clearClassList() {
    localStorage.removeItem(STORE_CLASSLIST);
}

function local_getClassList() {
    var list = localStorage.getItem(STORE_CLASSLIST);
    return list;
}

function local_storeClassList(list) {
    localStorage.setItem(STORE_CLASSLIST, JSON.stringify(list));
}

function local_hasMessageList() {
    return (local_getMessageList() !== null);
}

function local_clearMessageList() {
    localStorage.removeItem(STORE_MESSAGES);
}

function local_getMessageList() {
    return localStorage.getItem(STORE_MESSAGES);
}

function local_setMessageList(list) {
    localStorage.setItem(STORE_MESSAGES, JSON.stringify(list));
}