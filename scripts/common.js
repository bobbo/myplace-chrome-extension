var MYPLACE_URL = "http://classes.myplace.strath.ac.uk/";
var MYPLACE_VIEW_CLASS = "course/view.php?id=";

function httpGet(url, success_func, fail_func) {
    var xmlHttp = null;

    xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", url, true);
    xmlHttp.addEventListener("error", fail_func);
    xmlHttp.addEventListener("load", success_func);
    xmlHttp.send();
}